﻿using System;
using System.Text;
using Bogus;

namespace ConsoleApp10
{
    delegate void MistakeProcedure(StringBuilder personNote, Random random);
    class Program
    {  
        static void InsertRandomSymbol(StringBuilder personNote, Random random)
        {
            personNote.Insert(random.Next(personNote.Length), personNote[random.Next(personNote.Length)]);
        }
        static void RemovetRandomSymbol(StringBuilder personNote, Random random)
        {
            personNote.Remove(random.Next(personNote.Length), 1);
        }
        static void SwapSymbols(StringBuilder personNote, Random random)
        {
            int randomPosition = random.Next(personNote.Length - 1);
            personNote.Insert(randomPosition, personNote[randomPosition + 1]);
            personNote.Remove(randomPosition + 2, 1);
        }
        static void Main(string[] args)
        {
            try
            {
                if(args.Length < 2)
                {
                    throw new ArgumentException("Not enough arguments!");
                }

                Faker faker = new Faker(args[0]);
                StringBuilder[] personsNotes = new StringBuilder[int.Parse(args[1])];
                double mistakesPerNote = 0;

                if (args.Length == 3)
                {
                    if (!double.TryParse(args[2], out mistakesPerNote) || mistakesPerNote < 0)
                    {
                        throw new ArgumentException($"'{args[2]}' is incorrect argument for command!");
                    }
                }

                Random random = new Random();
                MistakeProcedure[] mistakeProcedures = { InsertRandomSymbol, RemovetRandomSymbol, SwapSymbols };

                for (int i = 0; i < personsNotes.Length; i++)
                {
                    int mistakesCount = (int)(mistakesPerNote - (int)mistakesPerNote <= random.NextDouble() ? mistakesPerNote : mistakesPerNote + 1);
                    personsNotes[i] = new StringBuilder($"{faker.Name.FullName()}; {faker.Address.FullAddress()}; {faker.Phone.PhoneNumber()}");
                    for (int j = 0; j < mistakesCount; j++)
                    {
                        mistakeProcedures[random.Next(3)](personsNotes[i], random);
                    }
                    Console.WriteLine(personsNotes[i]);
                }
            }catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
